This app is an advanced version of my previous todo application.
It is based on REST architectural style which  requires that a client make a request to the server in order to retrieve or modify data on the server.
Requests consists of a HTTP verb, a header, a path and a message body.
The application receives the request, and creates, modifies or returns a database entry (or entries) according to the content of the request.
