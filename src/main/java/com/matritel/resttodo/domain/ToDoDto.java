package com.matritel.resttodo.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Setter
@Builder
@Getter
public class ToDoDto {
    private String id;
    @Size(max = 255)
    @NotNull
    private String description;
    @Size(max = 40)
    @NotNull
    private String personInCharge;
    @NotNull
    private Priority priority;
    private LocalDate createdAt;

    public ToDoDto(String id, @Size(max = 255) @NotNull String description, @Size(max = 40) @NotNull String personInCharge, @NotNull Priority priority, LocalDate createdAt) {
        this.id = id;
        this.description = description;
        this.personInCharge = personInCharge;
        this.priority = priority;
        this.createdAt = createdAt;
    }
}
