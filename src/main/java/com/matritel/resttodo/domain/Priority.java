package com.matritel.resttodo.domain;

public enum Priority {
    URGENT, HIGH, MEDIUM, LOW
}

