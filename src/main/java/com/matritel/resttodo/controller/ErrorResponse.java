package com.matritel.resttodo.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter

public class ErrorResponse {
    private String message;
}
