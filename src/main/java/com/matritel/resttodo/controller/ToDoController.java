package com.matritel.resttodo.controller;


import com.matritel.resttodo.domain.Priority;
import com.matritel.resttodo.domain.ToDoDto;
import com.matritel.resttodo.service.ToDoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value = "api/matritel/todos")
public class ToDoController {
    @Autowired
    private ToDoService toDoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ToDoDto postToDo(@Valid @RequestBody ToDoDto todoInput) {
        return toDoService.create(todoInput);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ToDoDto> getTodos() {
        return toDoService.listToDos();
    }

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ToDoDto getTodoById(@PathVariable String id) {
        return toDoService.retrieveById(id).get();
    }

    @PutMapping(value = "/{id}")
    public ToDoDto putTodoById(HttpServletResponse response, @PathVariable String id, @Valid @RequestBody ToDoDto todoInput) {
        if (toDoService.retrieveById(id).isPresent()) {
            response.setStatus(200);
            return toDoService.update(id, todoInput);
        } else {
            response.setStatus(201);
            return toDoService.createWithId(id, todoInput);
        }
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteToDo(@PathVariable String id) {
        toDoService.deleteToDoById(id);
    }

    @GetMapping(value = "/findByPriority")
    @ResponseStatus(HttpStatus.OK)
    public List<ToDoDto> getAllToDoByPriority(@RequestParam(name = "priority") Priority priority) {
        return toDoService.listToDosByPriority(priority);
    }

    @GetMapping(value = "/findByPic")
    @ResponseStatus(HttpStatus.OK)
    public List<ToDoDto> getAllTodoByPic(@RequestParam(name = "pic") String pic) {
        return toDoService.listToDosByPic(pic);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse mapMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
        return new ErrorResponse("Wrong request: " + ex.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse mapMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        return new ErrorResponse("wrong argument: " + ex.getMessage());
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse mapMethodAEntityNotFoundException(EntityNotFoundException ex) {
        return new ErrorResponse("wrong argument: " + ex.getMessage());
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse mapExecption(Exception ex) {
        return new ErrorResponse("Internal Server Error: " + ex.getMessage());
    }
}
