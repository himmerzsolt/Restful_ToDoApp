package com.matritel.resttodo.service;


import com.matritel.resttodo.domain.Priority;
import com.matritel.resttodo.domain.ToDoDto;
import com.matritel.resttodo.repository.ToDoRepository;
import com.matritel.resttodo.repository.entity.ToDoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ToDoService {

    @Autowired
    private ToDoRepository repository;

    public ToDoDto update(String id, ToDoDto inputDto) {
        ToDoEntity toDoEntity = repository.getOne(id);
        toDoEntity.setDescription(inputDto.getDescription());
        toDoEntity.setPersonInCharge(inputDto.getPersonInCharge());
        toDoEntity.setPriority(inputDto.getPriority());
        toDoEntity.setCreatedAt(LocalDate.now());
        repository.save(toDoEntity);
        return toDto(toDoEntity);
    }

    public List<ToDoDto> listToDos() {
        List<ToDoEntity> entityList = repository.findAll();
        List<ToDoDto> dtoList = new ArrayList<>();
        for (ToDoEntity toDoEntity : entityList
        ) {
            dtoList.add(toDto(toDoEntity));
        }
        return dtoList;
    }

    public List<ToDoDto> listToDosByPic(String personInCharge) {
        List<ToDoEntity> list = repository.findAllByPersonInCharge(personInCharge);
        List<ToDoDto> dtoList = new ArrayList<>();
        for (ToDoEntity toDoEntity : list
        ) {
            dtoList.add(toDto(toDoEntity));
        }
        return dtoList;
    }

    public List<ToDoDto> listToDosByPriority(Priority priority) {
        List<ToDoEntity> list = repository.findAllByPriority(priority);
        List<ToDoDto> dtoList = new ArrayList<>();
        for (ToDoEntity toDoEntity : list
        ) {
            dtoList.add(toDto(toDoEntity));
        }
        return dtoList;
    }

    public void deleteToDoById(String id) {
        repository.deleteById(id);
    }

    public Optional<ToDoDto> retrieveById(String id) {
        Optional<ToDoEntity> toDoEntity = repository.findById(id);
        if (toDoEntity.isPresent()) {
            return Optional.of(toDto(toDoEntity.get()));
        } else
            return Optional.empty();
    }

    public ToDoDto create(ToDoDto todoInput) {
        ToDoEntity toSave = toEntity(todoInput);
        ToDoEntity saved = repository.save(toSave);
        return toDto(saved);
    }

    public ToDoDto createWithId(String Id, ToDoDto todoInput) {
        ToDoEntity toSave = toEntityWithId(Id, todoInput);
        ToDoEntity saved = repository.save(toSave);
        return toDto(saved);
    }

    private ToDoDto toDto(ToDoEntity saved) {
        return ToDoDto.builder()
                .id(saved.getId())
                .description((saved.getDescription()))
                .personInCharge(saved.getPersonInCharge())
                .priority(saved.getPriority())
                .createdAt(saved.getCreatedAt())
                .build();
    }

    private ToDoEntity toEntityWithId(String Id, ToDoDto todoInput) {
        return ToDoEntity.builder().id(Id)
                .description(todoInput.getDescription())
                .personInCharge(todoInput.getPersonInCharge())
                .priority(todoInput.getPriority())
                .createdAt(LocalDate.now())
                .build();
    }

    private ToDoEntity toEntity(ToDoDto todoInput) {
        return ToDoEntity.builder().description(todoInput.getDescription())
                .personInCharge(todoInput.getPersonInCharge())
                .priority(todoInput.getPriority())
                .createdAt(LocalDate.now())
                .build();
    }
}

