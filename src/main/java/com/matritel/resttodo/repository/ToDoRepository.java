package com.matritel.resttodo.repository;

import com.matritel.resttodo.domain.Priority;
import com.matritel.resttodo.repository.entity.ToDoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository
public interface ToDoRepository extends CrudRepository<ToDoEntity, String> {
    List<ToDoEntity> findAll();

    List<ToDoEntity> findAllByPriority(Priority Priority);

    ToDoEntity getOne(String id);

    List<ToDoEntity> findAllByPersonInCharge(String personInCharge);
}