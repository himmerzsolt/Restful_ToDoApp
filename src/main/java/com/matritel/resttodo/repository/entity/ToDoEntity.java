package com.matritel.resttodo.repository.entity;


import com.matritel.resttodo.domain.Priority;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;


@Entity(name = "todo")
@Builder
@Getter
@Setter
public class ToDoEntity {

    @Id
    @GenericGenerator(name = "UseExistingIdOtherwiseGenerateUsingIdentity", strategy = "com.matritel.resttodo.repository.UseExistingIdOtherwiseGenerateUsingIdentity")
    @GeneratedValue(generator = "UseExistingIdOtherwiseGenerateUsingIdentity")
    private String id;
    @Column(name = "description", columnDefinition = "VARCHAR(255)")
    private String description;
    @Column(name = "person_in_charge", columnDefinition = "VARCHAR(40)")
    private String personInCharge;
    @Enumerated(EnumType.STRING)
    @Column(length = 10)
    private Priority priority;
    @Column(name = "created_at")
    private LocalDate createdAt;

    public ToDoEntity() {
    }

    public ToDoEntity(String id, String description, String personInCharge, Priority priority, LocalDate createdAt) {
        this.id = id;
        this.description = description;
        this.personInCharge = personInCharge;
        this.priority = priority;
        this.createdAt = createdAt;
    }
}

